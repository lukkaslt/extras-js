# Categorizando valores com typeof e instanceof

Há dois operadores para categorizar valores: `typeof` para valores primitivos(ou não) e `instanceof` para objetos.

O uso do `typeof` se parece com:

```js
typeof value
```

Ele retorna uma string descrevendo o `"tipo"` do valor. Veja alguns exemplos:

```js
> typeof true
'boolean'
> typeof 'abc'
'string'
> typeof {}   // objeto literal vazio
'object'
> typeof []   // array literal vazio
'object'
```

A seguinte tabela mostra todos os resultados de `typeof`:

```
 Operando                         | Resultado
----------------------------------+------------------------------------------------
undefined                         | 'undefined'
----------------------------------+------------------------------------------------
null                              | 'object'
----------------------------------+------------------------------------------------
Boolean                           | 'boolean'
----------------------------------+------------------------------------------------
Number                            | 'number'
----------------------------------+------------------------------------------------
String                            | 'string'
----------------------------------+------------------------------------------------
Function                          | 'function'
----------------------------------+------------------------------------------------
Todos os outros valores normais   | 'object'
(Valores criados pelo motor)      | O mecanismo do JavaScript têm permissão 
                                  | para criar valores para os quais typeof 
                                  | retorna sequências arbitrárias (diferentes 
                                  | de todos os resultados listados nesta tabela).
----------------------------------+------------------------------------------------
```

`typeof null` retornando `'object'` é um bug que não pode ser corrigido, porque ele iria quebrar o código existente. Isso não significa que `NULL` seja um objeto.

O uso do `instanceof` se parece com:

```js
value instanceof Constr
```

Retorna `true` se o `valor` é um objeto criado pelo construtor `Constr`. Veja alguns exemplos:

```js
> var b = new Bar();  // objeto criado pelo construtor Bar
> b instanceof Bar
true

> {} instanceof Object
true
> [] instanceof Array
true
> [] instanceof Object  // Array é um 'subconstructor' de Object
true

> undefined instanceof Object
false
> null instanceof Object
false
```