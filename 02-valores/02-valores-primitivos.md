# Valores primitivos

Os seguintes são todos valores primitivos(ou somente *primitivos*):

* Booleans: `true`, `false`
* Numbers: `1994`, `1.78`
* Strings: `'abc'`, `"def"`
* Dois "nonvalues": `undefined`, `null`

Os primitivos possuem as seguintes características:

##### Comparáveis por valor

O "conteúdo" é comparável:

```js
> 3 === 3
true
> 'abc' === 'abc'
true
```

##### Sempre imutáveis

As propriedades não podem ser alteradas, adicionadas ou removidas:

```js
> var str = 'abc';

> str.length = 1; // tenta alterar a propriedade `length`
> str.length      // ⇒ no effect
3

> str.foo = 3;   // tenta criar a propriedade `foo`
> str.foo        // ⇒ no effect, unknown property
undefined
```

(O acesso a uma propriedade desconhecida sempre retorna `undefined`)
