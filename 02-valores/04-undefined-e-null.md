# Undefined e Null

A maioria das linguagens de programação possuem valores que denotam informações faltantes. O JavaScript possui dois destes "*nonvalues*": `undefined` e `null`:

`undefined` quer dizer *"Sem valor"*. Variáveis não inicializadas possuem `undefined` como valor:

```js
> var foo;
> foo
undefined
```

Paramêtros faltantes são `undefined`:

```js
> function f(x) { return x };
> f();
undefined
```

Se você tenta acessar uma propriedade não existente, recebe um `undefined`:

```js
> var obj = {};   // objeto vazio
> obj.foo
undefined
```

`null` significa "*não é objeto/diferente de objeto*". Ele é usado como um *"não-valor"* sempre que um objeto é esperado(paramêtros, o último em uma cadeia de objetos, etc.).

> `undefined` e `null` não possuem propriedades, e muito menos métodos padrão como `toString()`.

## Verificando se há undefined ou null

Funções normalmente permitem que você indique um valor ausente por meio de `undefined` ou `null`. Você pode fazer o mesmo atráves de uma verificação explícita:

```js
if ( x === undefined || x === null ) {
  ...
}
```

Você pode explorar o fato de que `undefined` e `null` são considerados `false` para encurtar a sintaxe da verificação:

```js
if ( !x ) {
  ...
}
```

> `false`, `0`, `NaN` e `''` são considerados `false`.

