# Valores

O JavaScript possui muitos dos valores que esperamos das linguagens de programação: Booleans, Numbers, Strings, Arrays, e assim por diante. Todos os valores em JavaScript possuem *propriedades*. Cada propriedade possui uma *chave*(ou *nome*) e um *valor*. Você pode pensar em propriedades como campos de um registro. Você usa o operador ponto(`.`) para acessar uma propriedade:

```js
value.propKey
```

Por exemplo, a string 'abc' possui a propriedade `length`:

```js
> var str = 'abc'
> str.length
3
```

O exemplo acima também pode escrito da seguinte maneira:

```js
> 'abc'.length
3
```

O operador *ponto* também é usado para atribuir um valor a uma propriedade:

```js
> var obj = {};   // objeto vazio
> obj.foo = 123;  // cria a prop. `foo`, e a atribui o num. 123
123
> obj.foo
123
```

Você também pode usá-los para invocar métodos:

```js
'hello'.toUpperCase()
'HELLO'
```

No exemplo anterior, invocamos o método `toUpperCase()` no valor `'hello'`.

