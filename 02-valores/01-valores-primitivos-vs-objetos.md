# Valores primitivos vs. Objetos

A distinção que o JavaScript faz entre os valor é um tanto quanto peculiar:

* Os *valores primitivos* são booleanos, numeros, strings, `null` e `undefined`.
* Todos os outros valores são `objetos`.

A grande diferença entre os dois é como eles são comparados: cada objeto tem uma identidade única e é apenas (estritamente) igual a si mesmo:

```js
> var obj1 = {};   // um objeto vazio
> var obj2 = {};   // outro objeto vazio
> obj1 === obj2;
false
> obj1 === obj1;
true
```

Em contrapartida, todos os valores primitivos contento o mesmo valor são considerados iguais:

```js
> var prim1 = 123;
> var prim2 = 123;
> prim1 === prim2
true
```

As próximas duas seções explicam objetos e valores primitivos com mais detalhes.

