# Objetos

Todos os valores *nonprimitive*(não primitivos) são *objetos*. Os tipos mais comuns de objetos são:

#### *Plain objects*, que podem ser criados mediante *object literals*:

```js
{
  firstName: 'Lucas',
  lastName: 'Lopes'
}
```

O objeto acima tem duas propriedades: o valor da propriedade `firstName` é `'Lucas'` e o valor da propriedade `lastName` é `'Lopes'`.

#### *Arrays*, podem ser criados mediante *arrays literals*

```js
[ 'Lucas', 'heisen', 23 ]
```

O array acima tem três propriedades que podem ser acessados mediante índices numéricos. Por exemplo, o índice de `'Lucas'` é `0`.

#### *Regular expressions*, podem ser criadas mediante *regular expressions literals*

```js
/^a+b+$/
```

Objetos possuem as seguintes características:

#### Comparáveis por referência

Sua identidade(referência única) é comparável; cada valor possui sua própria identidade:

```js
> {} === {}   // dois objetos vazios diferentes

> var obj1 = {};
> var obj2 = obj1;
> obj1 === obj2;
true
```

#### Mutáveis por padrão

Você tem a liberdade para alterar, remover e adicionar propriedades:

```js
> var obj = {};
> obj.foo = 123;  // adiciona a propriedade `foo`
> obj.foo
123
```