# Operadores

O JavaScript possui os seguintes operadores aritméticos:

* Adicão: `n1 + n2`
* Subtração: `n1 - n2`
* Multiplicação: `n1 * n2`
* Divisão: `n1 / n2`
* Resto da divisão: `n1 % n2`
* Incremento(sucessor): `++n, n++`
* Decremento(antecessor): `--n, n--`
* Negativo: `-n`
* Converte `n` para um número: `+n`
