# Operadores de atribuição compostos

Há operadores de atribuição compostoso, como `+=`. As duas atribuições a seguir são equivalentes:

```js
x += 1;
x = x + 1;
```
