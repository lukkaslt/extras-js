# Variáveis e atribuição

Variáveis em JavaScript são declaradas antes de serem usadas:

```js
var seno30; // declara a var. `seno30`
```
