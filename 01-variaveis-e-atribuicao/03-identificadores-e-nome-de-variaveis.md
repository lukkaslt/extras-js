# Identificadores e nome de variáveis

*Identificadores* são nomes que desempenham várias funções sintáticas em JavaScript. Por exemplo, o nome de uma variável é um identificador. Identificadores são sensíveis a maiúsculas e minúsculas(case-sensitive).

A grosso modo, o primeiro caractere de um identificador pode ser qualquer letra Unicode, um cifrão(`$`) ou um sublinhado(`_`). Caracteres subsequentes podem adicionalmente ser qualquer dígito(letras/numeros/caracteres especiais) Unicode. Assim, os seguintes identificadores são todos válidos:

```js
arg0
_tmp
$elem
```

Os identificadores a seguir são palavras reservadas - elas fazem parte da sintaxe e não podem ser usadas como nome de variáveis(incluindo nomes de funções e de paramêtros):

```
arguments  break    case       catch
class      const    continue   debugger
default    delete   do         else
enum       export   extends    false
finally    for      function   if
implements import   in         instanceof
interface  let      new        null
package    private  protected  public
return     static   super      switch
this       throw    true       try
typeof     var      void       while
```

Os três identificadores a seguir não são palavras reservadas, mas você deve tratá-los como se fossem:

```
Infinity
NaN
undefined
```
