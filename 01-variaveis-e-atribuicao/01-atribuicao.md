# Atribuição

Você pode declarar e atribuir um valor a uma variável ao mesmo tempo:

```js
var seno30 = 1 / 2;
```

Você também pode atribuir uma valor a uma variável existente:

```js
seno30 = 0.5; // altera o valor de `seno30`
```
