# Números

Todos os números em JavaScript são ponto-flutuante( floating-point ):

```js
> 1 === 1.0
true
```

Algumas peculiaridades devem ser observadas quanto aos números especiais:

#### NaN('*not a number*')

"Aquele valorzinho de erro":

```js
> Number('xyz')   // 'xyz' não pode ser convertido para um `number`
NaN
```

#### Infinity

"*Novamente mais um valorzinho de erro*":

```js
> 3 / 0
Infinity
> Math.pow(2, 1024)   // number too large
Infinity
```

`Infinity` é maior do que qualquer outro número ( exceto `NaN` ). Similarmente, -Infinity é menor do que qualquer outro número ( exceto `NaN` ). Isso torna esses números úteis quando usados como valores padrão ( por exemplo, quando você está procurando um mínimo ou um máximo ).

