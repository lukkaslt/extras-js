# Declarações VS. Expressões

location: https://gist.github.com/anonymous/4eac9b0177672f0a51b64d0902837326

Para compreender a sintaxe do JavaScript, você deve saber que ele possui duas categorias sintáticas principais: declarações e expressões:

* Declarações "fazem coisas". Um programa é uma sequência de declarações. Veja a seguir o exemplo de uma, que declara( cria ) uma variável `foo`:

```js
var foo;
```

* Expressões produzem valores. Estas são argumentos de função, o lado direito de uma atribuição, etc. Veja um exemplo de uma expressão:

```js
5 * ( 5 + 1 ) / 2;
```

A distinção entre declarações e expressões é melhor ilustrada pelo fato de podermos contruir **if-then-else** de duas maneiras - com uma declaração:

```js
var x;

if ( y >= 0 ) {
  x = y;
}
else {
  x = -y;
}
```

ou com uma expressão:

```js
// Maneira 1
var x = y >= 0 ? y : -y;

// Maneira 2
var w = z >= 0 && z || -z;
```

Você pode usar uma, das duas maneiras do exemplo acima(as expressões) como argumento de uma função(mas não pode usar uma declaração):

```js
myFunction( y >= 0 ? y : -y );
```

Resumindo, sempre que o JavaScript espera uma declaração, você também pode o dar uma expressão; por exemplo:

```js
foo( 7, 1 );
```

Toda linha é uma declaração( conhecida como *expression statement* ), mas a chamada à função `foo( 7, 1 )` é uma expressão.
