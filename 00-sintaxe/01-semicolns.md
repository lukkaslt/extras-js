# Ponto-e-vírgula

O uso do ponto-e-vírgula no JavaScript é opcional. No entanto, eu recomendo sempre incluí-los porque o JavaScript pode ficar confuso quanto ao final de uma declaração/expressão. Veremos tal comportamento mais adiante.

O ponto-e-vírgula indica o final de uma declaração, mas não de blocos. Há um caso em que você verá um ponto-e-vírgula após um bloco: em uma *function expression*. Se tal expressão é a que finaliza uma declaração, então ela é seguida por um ponto-e-vírgula:

```js
// Pattern: var _ = ___;
var x = 360 / 24;
var f = function () {}; // function expr. faz parte da decl var
```

# Comentários

O JavaScript tem dois tipos de comentários: comentários de linha única e comentários multilinha. Os comentários de linha única começam com `//` e são finalizados no final da linha:

```js
x++; // single-line comment
```

Comentários multilinha são delimitados por `/*` e `*/`:

```js
/* 
 Olaaaaa!
 Eu sou um comentario
 multilinha.
*/
```
