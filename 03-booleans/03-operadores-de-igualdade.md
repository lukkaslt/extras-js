# Operadores de Igualdade

O JavaScript possui dois tipos de igualdade:

* Normal, ou **"leniente"**, igualdade/desigualdade: `==` e `!=`
* Estrito igualdade/desigualdade: `===` e `!==`

A igualdade *Normal* "considera valores além da conta" como sendo iguais, fazendo com que bugs ocorram. Portanto, o uso da igualdade/desigualdade estrita é sempre a melhor opção.

