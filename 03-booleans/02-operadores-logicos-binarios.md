# Operadores Lógicos Binários

Os operadores lógicos binários funcionam como uma espécie de "*curto-circuito*"( short-circuiting ). Ou seja, se a avaliação do primeiro operando é suficiente para determinar o resultado, o segundo operando não é avaliado. Por exemplo, nas expressões a seguir, a função `bla()` nunca é chamada:

```js
false && bla()
true || bla()
```

Além disso, os operadores lógicos binários retornam um de seus operandos - que pode ou não ser um booleano. Uma "*checagem booleana*" pode ser feita para saber qual deles deve ser retornado:


#### And ( && )

Se o primeiro operando é **falsy**, o retorne. Caso contrário, retorne o segundo operando:

```js
> NaN && 'abc'
NaN
> 123 && 'abc'
'abc'
```

#### Or ( || )

Se o primeiro operando é **truthy**, o retorne. Caso contrário, retorne o segundo operando:

```js
> 'abc' || 123
'abc'
> '' || 123
123
```