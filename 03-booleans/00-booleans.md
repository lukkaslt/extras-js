# Booleans

O tipo primitivo `boolean` compreende os valores `true` e `false`. Os seguintes operadores produzem booleanos:

* Operadores lógicos binários: `&&` ( And ), `||` ( Or )
* Operador lógico prefixado: `!` ( Not )
* Operadores de comparação: 
  * Operadores de igualdade: `===`, `!==`, `==`, `!=`
  * Operadores relacionais(para strings e números): `>`, `>=`, `<`, `<=` 