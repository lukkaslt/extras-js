# Truthy e Falsy

Sempre que o JavaScript espera um valor booleano( por exemplo, a condição de uma declaração `if` ), qualquer valor pode ser usado. Tal será interpretado como `true` ou `false`. Os seguintes valores são interpretados como `false`:

* `undefined`, `null`
* Boolean: `false`
* Number: `-0`, `NaN`
* String: `''`

Todos os outros valores( incluindo todos os objetos! ) são interpretados como `true`. Valores interpretados como `false` são chamados de **falsy**, e valores interpretados como `true` são chamados de **truthy**. `Boolean()`, chamado como uma função, converte seu parâmetro em um booleano. Você pode usá-lo para verificar qual é o equivalente booleano de um valor qualquer:

```js
> Boolean( undefined )
false
> Boolean( 0 )
false
> Boolean( 3 )
true
> Boolean( {} )   // objeto vazio
true
> Boolean( [] )   // array vazio
true
```